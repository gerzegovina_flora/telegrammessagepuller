from telethon import TelegramClient
from datetime import datetime
import base64
from os import path
from pymongo import MongoClient
import shutil

from telethon.tl.functions.messages import GetHistoryRequest
from telethon.tl.types import MessageMediaPhoto, MessageMediaWebPage, MessageMediaDocument, MessageMediaPoll

api_id = 1752390
api_hash = '22d5bb681554f62b945ca8e56ee74feb'
client = TelegramClient('anon', api_id, api_hash)

mongo_client = MongoClient()
db = mongo_client['acme']
message_dates_snapshot_collection = db['messageDatesSnapshot']
message_collection = db['post']

channels = [
    'Женщина с косой',
    'TUT.BY новости',
    'Резидент',
    'Наблюдатель',
    'Легитимный',
    'Картель',
    'ЗеРада',
    'Недоторкані',
    'Крокодил'
]

channels_to_avatar = {

}

messages = []


async def main():
    channel_name_to_id = {}

    async for dialog in client.iter_dialogs():
        if dialog.name in channels:
            channel_name_to_id[dialog.name] = dialog.id

    while True:
        if path.exists('./imgs/'):  # and path.exists('./avatars/'):
            shutil.rmtree('./imgs/')
            # shutil.rmtree('./avatars/')
        message_snapshots = load_message_snapshot(channel_name_to_id)
        for message_snapshot in message_snapshots.values():
            channel_entity = await client.get_entity(message_snapshot['channelId'])
            messages_from_channel = await get_new_essages(channel_entity, message_snapshot)
            media_files = []

            messages_to_snapshot = []
            messages_to_store = []
            for message in messages_from_channel:
                if message.message == '' and isinstance(message.media, MessageMediaPhoto):
                    res = await client.download_media(message.media,
                                                      './imgs/')
                    media_files.append(to_base_64(res))
                else:
                    if message.media is not None \
                            and not (isinstance(message.media, MessageMediaWebPage)
                                     or isinstance(message.media, MessageMediaPoll)
                                     or isinstance(message.media, MessageMediaDocument)):
                        res = await client.download_media(message.media,
                                                          './imgs/')
                        media_files.append(to_base_64(res))

                    # Check, if avatar exists
                    avatar = channels_to_avatar[
                        channel_entity.title] if channel_entity.title in channels_to_avatar else await client.download_profile_photo(
                        channel_entity, file='./avatars/')
                    channels_to_avatar[channel_entity.title] = avatar
                    messages_to_snapshot.append({
                        "channelId": message_snapshot['channelId'],
                        "lastMessageId": message.id,
                        "channelName": channel_entity.title,
                    })

                    hash_tags, links = get_hash_tags_and_links(message.message)
                    messages_to_store.append({
                        "textMessage": message.message,
                        "mediaFiles": media_files,
                        "channelName": channel_entity.title,
                        "avatar": to_base_64(avatar),
                        "channelId": message_snapshot['channelId'],
                        "links": links,
                        "hashTags": hash_tags,
                        "timeCreation": datetime.now(),
                        "indexed": False,
                    })
                    media_files = []

            if len(messages_to_store) > 0:
                save_message_to_db(messages_to_store)
                last_message = messages_to_snapshot[0]
                update_last_message_id_for_channel(last_message)


def get_hash_tags_and_links(text):
    has_tags = [i for i in text.split() if i.startswith("#")]
    links = [i for i in text.split() if i.startswith("http")]
    return has_tags, links


def update_last_message_id_for_channel(last_message):
    channel_info = {
        'channelId': last_message['channelId'],
        'channelName': last_message['channelName'],
        'lastMessageId': last_message['lastMessageId'],
    }
    message_dates_snapshot_collection.delete_one({"channelId": last_message['channelId']})
    message_dates_snapshot_collection.insert_one(channel_info)


def save_message_to_db(messages_to_store):
    message_collection.insert_many(messages_to_store)


def load_channels():
    message_snapshots = {}
    for channel in message_dates_snapshot_collection.find():
        channel_info = {
            'channelId': channel['channelId'],
            'channelName': channel['channelName'],
            'lastMessageId': channel['lastMessageId'],
        }
        message_snapshots['channel_name'] = channel_info
    return message_snapshots


def load_message_snapshot(messages):
    message_snapshots = {}

    for msg_name in messages.keys():
        channels_from_db_count = message_dates_snapshot_collection.count_documents({"channelId": messages[msg_name]})

        if channels_from_db_count > 0:
            for channel in message_dates_snapshot_collection.find({"channelId": messages[msg_name]}):
                channel_info = {
                    'channelId': messages[msg_name],
                    'channelName': msg_name,
                    'lastMessageId': channel['lastMessageId'],
                }
                message_snapshots[channel['channelName']] = channel_info
        else:
            channel_info = {
                'channelId': messages[msg_name],
                'channelName': msg_name,
                'lastMessageId': 0,
            }
            message_snapshots[msg_name] = channel_info
    return message_snapshots


async def get_new_essages(channel_entity, message_snapshot):
    posts = await client(GetHistoryRequest(
        peer=channel_entity,
        limit=5,
        offset_date=None,
        offset_id=0,
        max_id=0,
        min_id=message_snapshot['lastMessageId'],
        add_offset=0,
        hash=0))

    return posts.messages


def get_avatar(channel_entity):
    pass


def to_base_64(file_path):
    if file_path is not None:
        with open(file_path, "rb") as image_file:
            return base64.b64encode(image_file.read()).decode("ascii")


with client:
    client.loop.run_until_complete(main())
